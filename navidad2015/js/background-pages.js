(function($){

	/* RESIZE TRIGGER
	 * Ejecuta los eventos onMobile y onDesktop al pasar por el punto de corte
	 */
	window.resizeTrigger = {
		modoMobile : true,
		puntoDeCorte : 769, // PUNTO DE CORTE
		mobileEvents : [],
		desktopEvents : [],
		inicializar : function(){
			// Ejecutar al iniciar
			// Alternar entre modo mobile y desktio
			this.modoMobile = $(window).width() < this.puntoDeCorte;
			// Alternar y ejecutar al redimencionar
			$(window).resize(function(){
				var widthScreen = $(this).width();
				if(widthScreen >= resizeTrigger.puntoDeCorte && resizeTrigger.modoMobile){
					resizeTrigger.ejecutar(resizeTrigger.desktopEvents);
					resizeTrigger.modoMobile = false;
				}else if(widthScreen < resizeTrigger.puntoDeCorte && !resizeTrigger.modoMobile){
					resizeTrigger.ejecutar(resizeTrigger.mobileEvents);
					resizeTrigger.modoMobile = true;
				}
			});
		},
		onMobile : function(evento){
			this.mobileEvents.push(evento);
			if(this.modoMobile){
				evento();
			}
			return this;
		},
		onDesktop : function(evento){
			if(!this.modoMobile){
				evento();
			}
			this.desktopEvents.push(evento);
			return this;
		},
		ejecutar : function(array){
			array.forEach(function(el,indice,array){
				array[indice]();
			});
		}
	};
	resizeTrigger.inicializar();
	/* FIN | RESIZE TRIGGER */


	$(window).load(function(){

		$('#content-inner').children().removeClass('stack-width');

		/* BLOQUE VIDEO */
		if($('body').hasClass('page-navidad-2015')){
			resizeTrigger.onDesktop(function(){
				$('.view-id-navidad_2015 .video-wrapper').css('background-image','url("'+$('.container-video').data('back-land')+'")');
			}).onMobile(function(){
				$('.view-id-navidad_2015 .video-wrapper').css('background-image','url("'+$('.container-video').data('back-port')+'")');
			});
		}

		/* BLOQUE LATAS */
		if($('body').hasClass('page-navidad-2015')){
			resizeTrigger.onDesktop(function(){
				$('.view-display-id-block_navidad_diseno').css('background-image','url("'+$('.container-diseno').data('back-land')+'")');
			}).onMobile(function(){
				$('.view-display-id-block_navidad_diseno').css('background-image','url("'+$('.container-diseno').data('back-port')+'")');
			});
		}

		/* BLOQUE RESERVA ESPECIAL */
		if($('body').hasClass('page-navidad-2015')){
			resizeTrigger.onDesktop(function(){
				$('.view-display-id-block_navidad_reserva').css('background-image','url("'+$('.container-reserva').data('back-land')+'")');
			}).onMobile(function(){
				$('.view-display-id-block_navidad_reserva').css('background-image','url("'+$('.container-reserva').data('back-port')+'")');
			});
		}

		/* BLOQUE RITUAL */
		if($('body').hasClass('page-navidad-2015')){
			resizeTrigger.onDesktop(function(){
				$('.view-display-id-block_ritual').css('background-image','url("'+$('.container-ritual').data('back-land')+'")');
			}).onMobile(function(){
				$('.view-display-id-block_ritual').css('background-image','url("'+$('.container-ritual').data('back-port')+'")');
			});
		}
	});

})(jQuery);