(function ($) {
	
	// Video popup
	$('body').append('<div id="popup" style="display: none;"><span class="close"></span></div>');

	var bpopup = null;
	$('.container-lata .link').live('click', function(e){
		var video = $(this).data('url');
		var videoID = video.split('?v=');
		$('#popup').append('<div class="videoWrapper"><iframe width="560" height="315" src="https://www.youtube.com/embed/'+videoID[1]+'?autoplay=1" frameborder="0" allowfullscreen></iframe></div>');
		bpopup = $('#popup').bPopup({
			onClose: function(){
				$('#popup .videoWrapper').remove();
			}
		});
	});

	$('#popup .close').live('click', function(){
		bpopup.close();
	});
	
})(jQuery);