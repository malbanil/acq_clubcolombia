
;(function($){
	$('a[href*=#]:not([href=#])').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if (target.length) {
				$('html,body').animate({
				scrollTop: target.offset().top
			}, 1000);
				return false;
			}
		}
	});

	// LATAS MOBILE
	function hideLatas(){
		var windowScreen = $(window).width();
		if(windowScreen > 769){
			$('.view-display-id-block_navidad_latas .container-general-lata').css('display','table-cell');
			$('.view-display-id-block_navidad_latas .latas-mobile').hide();

			$('.view-display-id-block_navidad_copas .container-general-copa').show();
			$('.view-display-id-block_navidad_copas .copas-mobile').hide();
		}
		else{
			$('.view-display-id-block_navidad_latas .container-general-lata').hide();
			$('.view-display-id-block_navidad_latas .latas-mobile').show();

			$('.view-display-id-block_navidad_copas .container-general-copa').hide();
			$('.view-display-id-block_navidad_copas .copas-mobile').show();
		}
	}

	function sortLatasMobile(){
		var lata_negra = $('.view-display-id-block_navidad_latas .view-content .latas-mobile .lata2641').clone().html();
		$('.view-display-id-block_navidad_latas .view-content .latas-mobile .lata2641').remove();
		$('.view-display-id-block_navidad_latas .view-content .latas-mobile').append('<div class="container-lata lata2641">'+lata_negra+'</div>');

		var copa_negra = $('.view-display-id-block_navidad_copas .view-content .copas-mobile .copa2666').clone().html();
		$('.view-display-id-block_navidad_copas .view-content .copas-mobile .copa2666').remove();
		$('.view-display-id-block_navidad_copas .view-content .copas-mobile').append('<div class="container-copa copa2666">'+copa_negra+'</div>');
	}

	$('.view-display-id-block_navidad_latas .view-content').append('<div class="latas-mobile"></div>');
	$('.view-display-id-block_navidad_copas .view-content').append('<div class="copas-mobile"></div>');
	$('.view-display-id-block_navidad_latas .container-general-lata').each(function(){
		var latas_desktop = $(this).clone().html();
		$('.view-display-id-block_navidad_latas .view-content .latas-mobile').append(latas_desktop);
	});
	$('.view-display-id-block_navidad_copas .container-general-copa').each(function(){
		var copas_desktop = $(this).clone().html();
		$('.view-display-id-block_navidad_copas .view-content .copas-mobile').append(copas_desktop);
	});
	sortLatasMobile();
	hideLatas();
	$(window).resize(function(){
		hideLatas();
	});

	var latas = $('.lata');

	latas.each( function(index, obj){
		var lata = $(this);
		var url = lata.attr('data-back-image');
		lata.html('<div id="animacion-' + index + '" class="animacion" style="background: url(' + url + ');"></div>')
		
		lata.mouseenter(function(){
			$('#animacion-' + index).css('background', 'none')
			$('#animacion-' + index).spritespin("frame", 0);
			$('#animacion-' + index).spritespin({
				source		: url, // path to sprite sheet
				width   	: 175,  // width in pixels of the window/frame
				height  	: 304,  // height in pixels of the window/frame
				frames		: 33,   // total number of frames
				framesX		: 11,
				loop		: false,
				stopFrame	: 32,
				behavior	: 'drag',
				renderer 	: 'background'
			});	
			$('#animacion-' + index).spritespin("animate", true);
		});
	})


})(jQuery);

